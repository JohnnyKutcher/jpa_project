package org.jpaproject.crud;

import org.jpaproject.entity.Seller;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class SellerService {

    public EntityManager em = Persistence.createEntityManagerFactory("SELLER").createEntityManager();

    public Seller add(Seller seller) {
        em.getTransaction().begin();
        Seller sellerFromDB = em.merge(seller);
        em.getTransaction().commit();
        return sellerFromDB;
    }

    public void delete(long id) {
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public Seller get(long id) {
        return em.find(Seller.class, id);
    }

    public void update(Seller seller) {
        em.getTransaction().begin();
        em.merge(seller);
        em.getTransaction().commit();
    }

    public List<Seller> getAll() {
        TypedQuery<Seller> namedQuery = em.createNamedQuery("Seller.getAll", Seller.class);
        return namedQuery.getResultList();
    }
}
