package org.jpaproject.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "sellers")
@NamedQuery(name =  "Seller.getAll", query = "SELECT c from Seller c")
public class Seller {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "fullName", length = 50)
    private String fullName;

    @Column(name = "age")
    private Integer age;

    @Column(name = "salary")
    private BigDecimal salary;

    public Seller() {
    }

    public Seller(String fullName, Integer age, BigDecimal salary) {
        this.fullName = fullName;
        this.age = age;
        this.salary = salary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Seller{" +
                "fullName='" + fullName + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }
}
