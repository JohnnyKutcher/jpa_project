package org.jpaproject.crud;

import org.jpaproject.entity.Seller;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class SellerServiceTest {

    SellerService service = new SellerService();
    @Test
    public void test() {
        Seller seller = new Seller();

        seller.setFullName("Ivan Kucher");
        seller.setAge(30);
        seller.setSalary(new BigDecimal(320));

        Seller seller1 = service.add(seller);
        System.out.println(seller1);
    }

    @Test
    public void testDeleteRecord() throws Exception {
        Seller seller = new Seller();

        seller.setFullName("Nikolai Kondratev");
        seller.setAge(28);
        seller.setSalary(new BigDecimal(300));

        Seller seller1 = service.add(seller);

        service.delete(seller1.getId());
        service.delete(2);
    }

    @Test
    public void testSelect() throws Exception {
        Seller seller = new Seller();

        seller.setFullName("Anton Anton");
        seller.setAge(32);
        seller.setSalary(new BigDecimal(295));


        Seller seller1 = service.add(seller);

        Seller sellerFromDB = service.get(seller1.getId());
        System.out.println(sellerFromDB);
    }

    @Test
    public void testUpdate() throws Exception {
        Seller seller = new Seller();

        seller.setFullName("Alex Alex");
        seller.setAge(31);
        seller.setSalary(new BigDecimal(276));


        Seller seller1 = service.add(seller);

        seller1.setSalary(new BigDecimal(0));

        service.update(seller1);

        Seller seller2 = service.get(seller1.getId());
        System.out.println(seller2);

    }

    @Test
    public void testGetAll() {
        Seller seller = new Seller();
        seller.setFullName("Jui Mui");
        seller.setAge(37);
        seller.setSalary(new BigDecimal(15));

        Seller seller1 = new Seller();
        seller1.setFullName("Richsha Pu");
        seller1.setAge(45);
        seller1.setSalary(new BigDecimal(7));

        service.add(seller);
        service.add(seller1);

        List<Seller> sellerList = service.getAll();

        for (Seller s: sellerList) {
            System.out.println(s);
        }
    }
}
